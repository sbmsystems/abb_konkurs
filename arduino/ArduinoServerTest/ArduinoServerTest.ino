#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {
  0x90, 0xA2, 0xDA, 0x0D, 0x1E, 0x2B
};

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;
IPAddress SERVER (192, 168, 123, 123);
const int PORT = 1994;
int sleepCounter;
int temperature;
void setup() {
  delay (2000);
  sleepCounter = 0;
  temperature = 40;

  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
  }
  // print your local IP address:
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }
  Serial.println();
}

void printTermometr(){
  if (client.connect(SERVER, PORT)){
    Serial.println("connected");
    client.println("Arduino");
    client.println("Sensor");
    client.println("Thermometer");
    client.println("5");
    client.println(temperature);
    temperature += 30;
  } 
}
void sendPre(){
  Serial.println("connecting...");
  if (client.connect(SERVER, PORT)) {
    Serial.println("connected");
    client.println("Arduino");
    client.println("Presence Sensor");
    client.println("3");
    client.println("1");
    client.stop();
  } else {
    Serial.println("Connection failed");
  }
}
void sendGAS(){
  Serial.println("connecting...");
  if (client.connect(SERVER, PORT)) {
    Serial.println("connected");
    client.println("Arduino");
    client.println("GAS");
    client.println("2");
    client.println("0");
    client.stop();
  } else {
    Serial.println("Connection failed");
  }
}
void sendTemp(){
  Serial.println("connecting...");
  if (client.connect(SERVER, PORT)) {
    Serial.println("connected");
    client.println("Arduino");
    client.println("Thermometer");
    client.println("1");
    client.println(temperature);
    temperature+=5;
    client.stop();
  } else {
    Serial.println("Connection failed");
  }
}
void sendAll() {
  sendTemp();
  sendGAS();
  sendPre();
}
void loop() {
  if (sleepCounter == 50) {
    sendAll();
    sleepCounter = 0;
    Serial.println("Data send");
  }
  else{
    sleepCounter++;
  }
  delay(100);
}

