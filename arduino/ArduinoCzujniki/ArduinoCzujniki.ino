/* Variables setup*/
const int presenceAnalogPIN = 0;
const int thermometerAnalogPIN = 2;
const int gasAnalogPIN = 4;

float temperature;
float gas;
int presence;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void getTemp(){
 int reading = analogRead(2);  
 Serial.print("Voltage is ");
 Serial.println(reading);
 float voltage = (reading *5.0) / 1024 ;
 
 float temperatureC = ((voltage - 0.5) * 100);
 
 Serial.print(temperatureC); Serial.println(" degrees C");
 
 //return (temperatureC);
}
/**
* 1 - There is gas 0 - there is no gas
*/
int getGas(){
    int state = analogRead(gasAnalogPIN);
    Serial.print("Gas sensor reading is "); 
    Serial.println(state);
  
}


void loop() {
  getTemp();
  getGas();
 delay(500);
}
