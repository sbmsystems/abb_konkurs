package abb.konkurs.sbm.auth;

/**
 * @author Alex Baranowski
 * @version 1.0 Desc: Ta klasa bedzie wspoldzielona przez wszystkie metody
 *          authentykacji!!! Domyslnie klasa wywoluje metode equals().
 */
public final class Authenticator<T1> {
    public Authenticator() {
    }

    /**
     * @param  argumenty tej samej klasy
     * 
     * @return Zwraca true jesli argumenty sa takie same.
     */
    public boolean authenticate(T1 arg1, T1 arg2) {
        if (arg1.equals(arg2))
            return true;
        else
            return false;
    }

    /**
     * @param Dwa argumenty tej samej klasy, obiekt implementujacy
     * ComperableForTwo, ktorego metoda bedzie wywolna na 2 obiektach
     * 
     * @return Zwraca true jesli compeareTo(arg1,arg2) zwraca 0
     */
    public boolean auth2(T1 arg1, T1 arg2, ComperableForTwo<T1> authMetod) {
        if (authMetod.compareTo(arg1, arg2) == 0)
            return true;
        return false;

    }
}
