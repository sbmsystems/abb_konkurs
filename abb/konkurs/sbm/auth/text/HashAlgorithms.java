package abb.konkurs.sbm.auth.text;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

/**
 * 
 * @author Alex
 * @version 1.0
 * </p> Klasa implementuje algorytmy haszujace</p>
 */
public class HashAlgorithms {

    public static final int DEFAULT_SALT_LENGTH = 7;
    public static String SHA256_STRING = "SHA-256";
    public static String SHA384_STRING = "SHA-384";
    public static String SHA512_STRING = "SHA-512";
    public static String SHA1_STRING = "SHA-1";
    public static String MD5_STRING = "MD5";
    public static final int SHA256 = 1;
    public static final int SHA384 = 2;
    public static final int SHA512 = 3;
    public static final int SHA1 = 4;
    public static final int MD5 = 5;
    public static final int DEFAULT_HASHING_METHOD = 2;

    public static String generateHash(int hashMethod, String toHash) {
        if (hashMethod == 1){
            return getHashSHA256(toHash);
        } else if (hashMethod == 2){
            return getHashSHA384(toHash);
        }else if (hashMethod == 3){
            return getHashSHA512(toHash);
        }else if (hashMethod == 4){
            return getHashSHA1(toHash);
        }else if (hashMethod == 5){
            return getHashMD5(toHash);
        }
        return null;
    }

    /**
     * @param howLong
     * @return random generated [a-zA-Z] string
     */
    public static String generateSalt(int howLong) {
        if (howLong <= 0 || howLong > 1000)
            return null;
        Random rand = new Random(System.currentTimeMillis());
        StringBuilder str = new StringBuilder();
        char[] tab = new char[52];
        for (int i = 0; i < 52; i++) {
            tab[i] = (char) ('a' + i);
            tab[i] = (char) ('A' + i);
        }
        for (int i = 0; i < howLong; i++) {
            str.append(tab[rand.nextInt(52)]);
        }
        return str.toString();
    }

    /**
     * 
     * @param toHash
     * @return return SHA256 hash
     * @throws UnsupportedEncodingException
     */
    public static String getHashSHA256(String toHash) {
        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance(SHA256_STRING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("Nie ma takiego algorytmu" + SHA256_STRING);
        }
        if (algorithm == null)
            return null;

        try {
            algorithm.update(toHash.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {

        }
        String hex = (new HexBinaryAdapter().marshal(algorithm.digest()));
        algorithm.reset();
        return hex;
    }

    /**
     * 
     * @param toHash
     * @return return SHA512 hash
     * @throws UnsupportedEncodingException
     */
    public static String getHashSHA512(String toHash) {
        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance(SHA512_STRING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("Nie ma takiego algorytmu" + SHA512);
        }
        if (algorithm == null)
            return null;
        try {
            algorithm.update(toHash.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {

        }
        String hex = (new HexBinaryAdapter().marshal(algorithm.digest()));
        algorithm.reset();
        return hex;
    }

    /**
     * 
     * @param toHash
     * @return return SHA384 hash
     * @throws UnsupportedEncodingException
     */
    public static String getHashSHA384(String toHash) {
        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance(SHA384_STRING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("Nie ma takiego algorytmu" + SHA384);
        }
        if (algorithm == null)
            return null;
        try {
            algorithm.update(toHash.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {

        }
        String hex = (new HexBinaryAdapter().marshal(algorithm.digest()));
        algorithm.reset();
        return hex;
    }

    /**
     * 
     * @param toHash
     * @return return SHA1 hash
     * @throws UnsupportedEncodingException
     */
    public static String getHashSHA1(String toHash) {
        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance(SHA1_STRING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("Nie ma takiego algorytmu" + SHA1);
        }
        if (algorithm == null)
            return null;
        try {
            algorithm.update(toHash.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {

        }
        String hex = (new HexBinaryAdapter().marshal(algorithm.digest()));
        algorithm.reset();
        return hex;
    }

    /**
     * 
     * @param toHash
     * @return return MD5 hash
     * @throws UnsupportedEncodingException
     */
    public static String getHashMD5(String toHash) {
        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance(MD5_STRING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("Nie ma takiego algorytmu" + MD5);
        }
        if (algorithm == null)
            return null;
        try {
            algorithm.update(toHash.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {

        }
        String hex = (new HexBinaryAdapter().marshal(algorithm.digest()));
        algorithm.reset();
        return hex;
    }
}
