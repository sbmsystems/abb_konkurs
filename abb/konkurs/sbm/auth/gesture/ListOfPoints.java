package abb.konkurs.sbm.auth.gesture;

import java.util.ArrayList;

/**
 * 
 * @author Alex
 * @version 1.0 Desc: Ta klasa trzyma liste punktow
 */
public class ListOfPoints {
    ArrayList<Point> listOfPoints;

    public ListOfPoints() {
        listOfPoints = new ArrayList<Point>();
    }

    public void clear() {
        listOfPoints.clear();
    }

    public int getSize() {
        return listOfPoints.size();
    }

    public boolean addElement(Point p) {
        return listOfPoints.add(p);
    }

    /**
     * @param index
     * @return Point if index is correct, null if index is out of bound
     */
    public Point getElement(int index) {
        if (index < listOfPoints.size() && index >= 0) {
            return listOfPoints.get(index);
        } else
            return null;
    }

    /**
     * @param Object o
     * @return true if there is number of points in list is the same and all
     *         elements are equal (NOTE: order is important!)
     */
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o instanceof ListOfPoints) {
            ListOfPoints lop = (ListOfPoints) o;
            if (lop.getSize() != this.getSize())
                return false;
            else {
                for (int i = 0; i < lop.getSize(); i++) {
                    if (!(lop.getElement(i).equals(this.getElement(i))))
                        return false;
                }
            }
        } else
            return false;
        return true;
    }

}
