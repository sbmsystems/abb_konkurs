package abb.konkurs.sbm.auth.gesture;

/**
 * @author Alex
 * @version 1.0 Desc: Ta klasa odpowiada za tworzenie punktu o 2 wspolrzednych.
 *          Java posiada gdzies taka klase w sobie, ale nie chce mi sie jej
 *          szukac ;).
 */
public class Point {
    private final int x;
    private final int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (o instanceof Point) {
            Point p = (Point) o;
            if (p.x != this.x || p.y != this.y)
                return false;
        } else
            return false;
        return true;
    }

}
