package abb.konkurs.sbm.auth;

/**
 * @author Alex
 * @version 1.0
 * @param <T1> Typ obiektu ktory ten obiekt bedzie porownywal
 */
public interface ComperableForTwo<T1> {
    /**
     * Ta funkcja odpowiada za porownanie 2 obiektow
     * 
     * @param T1 arg1, T1 arg2
     * @return 0 if objects are equal for method sth<0 jesli pierwszy jest
     *         mniejszy. sth>0 jesli pierwszy jest wiekszy.
     */
    public int compareTo(T1 arg1, T1 arg2);

}
