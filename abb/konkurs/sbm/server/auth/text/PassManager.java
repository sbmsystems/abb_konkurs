package abb.konkurs.sbm.server.auth.text;

/**
 * @author Alex
 * @version 1.1 
 * <p>PassManager implements singleton of <b>PasswordTextFileManager</b></p>
 */


public class PassManager {
   private static final PasswordTextFileManager MAN = new PasswordTextFileManager(); 
   public static PasswordTextFileManager getManager(){ return MAN;}
}
