package abb.konkurs.sbm.server.auth.text;
/**
 * @author Alex
 * @version 1.1 
 * <p>WordsPassManager implements singleton of <b>WordsPasswordTextFileManager</b></p>
 */

public class WordsPassManager {
   private static final WordsPassowrdTextFileManager MAN = new WordsPassowrdTextFileManager();
   public static WordsPassowrdTextFileManager getManager(){ return MAN;}
}
