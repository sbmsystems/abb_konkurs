package abb.konkurs.sbm.server.auth.text;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import abb.konkurs.sbm.server.ServerMain;

/**
 * 
 * @author Alex
 *         <p>
 *         Klasa ta odpowiada za tworzenie i management plikiem hasel
 *         username:hashMethod:salt:HASH default hash method SHA256
 *         </p>
 */
public class PasswordTextFileManager {
    private List<String> lista;
    private Path path;
    public static final int SHA256 = 1;
    public static final int SHA384 = 2;
    public static final int SHA512 = 3;
    public static final int SHA1 = 4;
    public static final int MD5 = 5;
    public static final int DEFAULT_HASHING_METHOD = 2;
    final static Charset ENCODING = StandardCharsets.UTF_8;

    protected PasswordTextFileManager() {
        lista = new ArrayList<String>();
        path = Paths.get(ServerMain.VOICE_PASS_FILE);

        File file = new File(ServerMain.VOICE_PASS_FILE);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            readFromFile();
    }

    public boolean validUserName(String username) {
        if (username == null || username.isEmpty())
            return false;
        return username.matches("[a-zA-Z]+");
    }

    public void addUser(String username, String salt, String hash) {
        StringBuffer strbuf = new StringBuffer();
        strbuf.append(username);
        strbuf.append(":2:");
        strbuf.append(salt);
        strbuf.append(":");
        strbuf.append(hash);
        lista.add(strbuf.toString());
        writeToFile();
    }

    public void addUser(String username, int hashMethod, String salt, String hash) {
        StringBuffer strbuf = new StringBuffer();
        strbuf.append(username);
        strbuf.append(":");
        strbuf.append(hashMethod);
        strbuf.append(":");
        strbuf.append(salt);
        strbuf.append(":");
        strbuf.append(hash);
        lista.add(strbuf.toString());
        writeToFile();
    }

    public String[] getAllUsers() {
        readFromFile();
        if (lista.size() <= 0)
            return null;
        String[] users = new String[lista.size()];
            int i = 0;
        for (String s : lista) {
            for (String x : s.split(":")) {
                users[i] = x;
                i++;
                break;
            }
        }
        return users;
    }

    public void changeUserPassword(String username, int hashMethod, String salt, String hash) {
        deleteUser(username);
        if (hashMethod == -1) {
            addUser(username, salt, hash);
        } else {
            addUser(username, hashMethod, salt, hash);
        }

    }

    public void deleteUser(String username) {
        readFromFile();
        int index = -1;
        int index2 = 0;
        for (String s : lista) {
            if (index != -1)
                break;
            int i = 0;
            for (String x : s.split(":")) {
                if (i == 0) {
                    i++;
                    if (x.equals(username))
                        index = index2;
                } else
                    break;
            }
            index2++;
        }
        if (index != -1) {
            lista.remove(index);
            writeToFile();
        }
    }

    public boolean userAlreadyInDB(String username) {
        readFromFile();
        for (String s : lista) {
            int i = 0;
            for (String x : s.split(":")) {
                if (i == 0) {
                    if (x.equals(username))
                        return true;
                    i++;
                } else
                    break;
            }
        }
        return false;
    }

    public String userHash(String username) {
        readFromFile();
        for (String s : lista) {
            int i = 0;
            boolean rightUser = false;
            for (String x : s.split(":")) {
                if (i == 0) {
                    if (x.equals(username))
                        rightUser = true;
                }
                if (i == 3 && rightUser == true) {
                    return x;
                }
                i++;
            }
        }
        return null;
    }

    public String userSalt(String username) {
        readFromFile();
        for (String s : lista) {
            int i = 0;
            boolean rightUser = false;
            for (String x : s.split(":")) {
                if (i == 0) {
                    if (x.equals(username))
                        rightUser = true;
                }
                if (i == 2 && rightUser == true) {
                    return x;
                }
                i++;
            }
        }
        return null;
    }

    public String userHashMethod(String username) {
        readFromFile();
        for (String s : lista) {
            int i = 0;
            boolean rightUser = false;
            for (String x : s.split(":")) {
                if (i == 0) {
                    if (x.equals(username))
                        rightUser = true;
                }
                if (i == 1 && rightUser == true) {
                    return x;
                }
                i++;
            }
        }
        return null;
    }

    private synchronized void writeToFile() {
        try {
            Files.write(path, lista, ENCODING);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private synchronized void readFromFile() {
        try {
            lista = Files.readAllLines(path);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
