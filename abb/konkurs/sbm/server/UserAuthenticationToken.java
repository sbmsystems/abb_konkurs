package abb.konkurs.sbm.server;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class UserAuthenticationToken {
	private long token;
	private String username;
	private Date lastUsageTime;

	public UserAuthenticationToken(String username, long token) {
		this.username = username;
		this.token = token;
		this.lastUsageTime = new Date();
	}	
	public String getUsername() {
		return username;
	}

	public long getToken() {
		return token;
	}

	public synchronized boolean tokenStillValid(){
		if (getDateDiff(lastUsageTime, new Date(), TimeUnit.MINUTES) > 9)
			return false;
		else
				return true;
	}
	public synchronized void resetTime(){
		lastUsageTime = new Date();
	}
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

}
