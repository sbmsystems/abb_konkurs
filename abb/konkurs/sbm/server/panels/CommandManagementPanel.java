package abb.konkurs.sbm.server.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import abb.konkurs.sbm.server.CommandManagementFrame;
import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.commands.Command;
import abb.konkurs.sbm.server.commands.LockDoorCommand;
import abb.konkurs.sbm.server.commands.TurnOffCommand;
import abb.konkurs.sbm.server.commands.TurnOnCommand;
import abb.konkurs.sbm.server.commands.UnlockDoorCommand;

public class CommandManagementPanel extends JPanel {

	private static final long serialVersionUID = 1291286875123123L;
	public static final String TITLE = "Sensor Management";
	private static String commandAddCommand = "ADD COMMAND";
	private static String commandMoreInfo = "LIST COMMAND";
	private static String commandDeleteCommand = "DELETE COMMAND";
	private static String ERROR_DELTE = "Cannot delete not-user-defined command";
	private static String ERROR_NO_COMMANDS_AVALIABLE = "There is no commands avaliable";
	private JButton addCommandButton;
	private JButton moreInfoButton;
	private JButton deleteCommandButton;
	private JList<String> commandLIst;
	private Command[] senArry;

	public CommandManagementPanel() {
		super();
		ServerMain.addCommand(new TurnOffCommand("Turn on Command"));
		ServerMain.addCommand(new TurnOnCommand("Turn off Command"));
		ServerMain.addCommand(new LockDoorCommand("Lock door Command"));
		ServerMain.addCommand(new UnlockDoorCommand("Unlock door Command"));
		generateUI();
	}

	public void refreshGUI() {
		fillSensorList();
	}

	private void generateUI() {
		addCommandButton = new JButton(commandAddCommand);
		moreInfoButton = new JButton(commandMoreInfo);
		deleteCommandButton = new JButton(commandDeleteCommand);
		commandLIst = new JList<String>();
		initElements();
	}

	private void initElements() {
		/* Setting Action command */
		addCommandButton.setActionCommand(commandAddCommand);
		moreInfoButton.setActionCommand(commandMoreInfo);
		deleteCommandButton.setActionCommand(commandDeleteCommand);
		/* setting dimensions */
		moreInfoButton.setPreferredSize(new Dimension(150, 50));
		addCommandButton.setPreferredSize(new Dimension(150, 50));
		deleteCommandButton.setPreferredSize(new Dimension(150, 50));
		commandLIst.setPreferredSize(new Dimension(400, 150));

		/* making button Panel */
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(addCommandButton);
		buttonPanel.add(moreInfoButton);
		buttonPanel.add(deleteCommandButton);
		/* Dodanie actionlistnerow */
		deleteCommandButton.addActionListener(new MyListener());
		addCommandButton.addActionListener(new MyListener());
		moreInfoButton.addActionListener(new MyListener());
		/* Ustawienie Layoutu */
		this.setLayout(new BorderLayout());
		this.add(buttonPanel, BorderLayout.PAGE_START);
		fillSensorList();
		this.add(commandLIst, BorderLayout.PAGE_END);

	}

	private void fillSensorList() {
		senArry = ServerMain.getAllCommands();
		if (senArry == null || senArry.length <= 0) {
			System.out.println("## PUSTO TO JAK CHOLERA");
			commandLIst.setListData(new String[0]);
			return;
		}

		String[] arr = new String[senArry.length];
		for (int i = 0; i < senArry.length; i++) {
			arr[i] = senArry[i].getDesc();
		}
		commandLIst.setListData(arr);
		commandLIst.setSelectedIndex(0);
		commandLIst.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	private int getIndex() {
		if (commandLIst.isSelectionEmpty())
			return -1;
		return commandLIst.getSelectedIndex();
	}

	class MyListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int index = getIndex();
			if (arg0.getActionCommand().equals(commandAddCommand)) {
				if (senArry.length > 0){
				JFrame frame = new CommandManagementFrame();
                frame.setAlwaysOnTop(true);
				}else{
						JOptionPane.showMessageDialog(null, ERROR_NO_COMMANDS_AVALIABLE,"ERROR",
							JOptionPane.ERROR_MESSAGE);
				}
			} else if (arg0.getActionCommand().equals(commandMoreInfo)) {
				if (index < 0) {
					return;
				}
				JOptionPane.showMessageDialog(null,
						senArry[index].getDesc());
			} else if (arg0.getActionCommand().equals(commandDeleteCommand)) {
				if (index < 0) {
					return;
				}
				//It's impossible to delte basic command 
				if (senArry[index].isLeaf() == true){
					JOptionPane.showMessageDialog(null, ERROR_DELTE,"ERROR",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				ServerMain.removeCommand(index);
			}
		}
	}

}
