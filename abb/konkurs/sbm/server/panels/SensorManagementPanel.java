package abb.konkurs.sbm.server.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import abb.konkurs.sbm.sensors.Sensor;
import abb.konkurs.sbm.server.ServerMain;

/**
 * 
 * @author Alex
 * @version 1.1
 *          <p>
 *          This class is simple jpanel, which is part of ServerMainFrame card
 *          layout
 *          </p>
 */
public class SensorManagementPanel extends JPanel {
	private static final long serialVersionUID = -6974650950955973291L;
	public static final String TITLE = "Sensor Management";
	private static String commandRefresh = "REFRESH STATE";
	private static String commandMoreInfo = "MORE INFO";
	private static String commandRefreshAll = "REFRESH STATE OF ALL SENSORS";
	private JButton refreshStateButton;
	private JButton moreInfoButton;
	private JButton refreshAllButton;
	private JList<String> sensorList;
	private Sensor[] senArry;

	public SensorManagementPanel() {
		super();
		generateUI();
	}

	public void refreshGUI() {
		fillSensorList();
	}

	private void generateUI() {
		refreshStateButton = new JButton(commandRefresh);
		moreInfoButton = new JButton(commandMoreInfo);
		refreshAllButton = new JButton(commandRefreshAll);
		sensorList = new JList<>();
		initElements();
	}

	private void initElements() {
		/* Setting Acction commadn */
		refreshStateButton.setActionCommand(commandRefresh);
		moreInfoButton.setActionCommand(commandMoreInfo);
		refreshAllButton.setActionCommand(commandRefreshAll);
		/**/
		refreshStateButton.setPreferredSize(new Dimension(230, 50));
		moreInfoButton.setPreferredSize(new Dimension(230, 50));
		refreshAllButton.setPreferredSize(new Dimension(400, 50));
		sensorList.setPreferredSize(new Dimension(400, 150));
		JPanel buttonPanel1 = new JPanel();
		buttonPanel1.add(refreshStateButton);
		buttonPanel1.add(moreInfoButton);
		JPanel buttonPanel2 = new JPanel();
		buttonPanel2.add(refreshAllButton);
		/* Dodanie actionlistnerow */
		refreshStateButton.addActionListener(new MyListener());
		moreInfoButton.addActionListener(new MyListener());
		refreshAllButton.addActionListener(new MyListener());
		/* Ustawienie Layoutu */
		this.setLayout(new BorderLayout());
		this.add(buttonPanel1, BorderLayout.PAGE_START);
		this.add(buttonPanel2, BorderLayout.CENTER);
		fillSensorList();
		this.add(sensorList, BorderLayout.PAGE_END);

	}

	/* Cold hard bitch just a kiss on the lips */

	private void fillSensorList() {
		senArry = ServerMain.getAllSensors();
		if (senArry == null || senArry.length <= 0) {
			sensorList.setListData(new String[0]);
			return;
		}
	
		String[] arr = new String[senArry.length];
		for (int i = 0; i < senArry.length; i ++){
			 arr[i] = senArry[i].getDesc();
		}
		sensorList.setListData(arr);
		sensorList.setSelectedIndex(0);
		sensorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	private int getIndex() {
		if (sensorList.isSelectionEmpty())
			return -1;
		return sensorList.getSelectedIndex();
	}

	class MyListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (arg0.getActionCommand().equals(commandRefresh)) {
				if (getIndex() < 0) {
					return;
				}
			} else if (arg0.getActionCommand().equals(commandMoreInfo)) {
				if (getIndex() < 0) {
						return;	
				}else{
					Sensor s = senArry [getIndex()];
					JOptionPane.showMessageDialog(null,
							s.getDesc()+"\nID: "+s.getID()+"\nDATA: "+s.getData());
				
				}
			} else if (arg0.getActionCommand().equals(commandRefreshAll)) {
				if (getIndex() < 0) {
					ServerMain.refreshSensors();
				}
			}
		}
	}

}
