package abb.konkurs.sbm.server.panels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import abb.konkurs.sbm.auth.text.HashAlgorithms;
import abb.konkurs.sbm.server.auth.text.PassManager;

/**
 * 
 * @author Alex
 * @version 1.0
 *          <p>
 *          This frame is created when user want to change password.
 *          </p>
 */
public class UserChangePasswordFrame extends JFrame {
	private static final long serialVersionUID = 5586512542984714489L;
	private static final String TITLE = "Change Passowrd";
	private static final String CMD_CHANGE = "CHANGE";
	private static final String CMD_EXIT = "EXIT";
	private static final int PassFieldLength = 12;
	private final JLabel passLabel = new JLabel("New password :");
	private JPanel panel;
	private JPasswordField passField;
	private JFrame IFrame;
	private String userName;

	public UserChangePasswordFrame(String userName) {
		super();
		this.userName = userName;
		setTitle(TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		initElements();
		IFrame = this;
	}

	private void initElements() {
		panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		passField = new JPasswordField(PassFieldLength);
		passLabel.setLabelFor(passField);

		panel.add(passLabel);
		panel.add(passField);
		panel.add(createButtonPanel());
		this.add(panel);
		this.pack();
		this.setVisible(true);
	}

	protected JComponent createButtonPanel() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JButton okButton = new JButton(CMD_CHANGE);
		JButton exitButton = new JButton(CMD_EXIT);

		okButton.setActionCommand(CMD_CHANGE);
		exitButton.setActionCommand(CMD_EXIT);

		okButton.addActionListener(new MyListener());
		exitButton.addActionListener(new MyListener());

		p.add(okButton);
		p.add(exitButton);
		return p;
	}

	class MyListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals(CMD_CHANGE)) {
				char[] pass;
				pass = passField.getPassword();
				if (pass.length < 6) {
					JOptionPane.showMessageDialog(IFrame,
							"Password musts be longer than 5 characters");
					return;
				}
				// PassManager.getManager().addUser(gt, ashMethod, salt, hash);
				String salt = HashAlgorithms
						.generateSalt(HashAlgorithms.DEFAULT_SALT_LENGTH);
				PassManager.getManager().deleteUser(userName);
				PassManager.getManager().addUser(userName, salt,
						HashAlgorithms.getHashSHA256(new String(pass) + salt));
				JOptionPane.showMessageDialog(IFrame, "User password changed");
				IFrame.setVisible(false);
				IFrame.dispose();
				
			} else if (e.getActionCommand().equals(CMD_EXIT)) {
				IFrame.setVisible(false);
				IFrame.dispose();
			}
		}

	}

}
