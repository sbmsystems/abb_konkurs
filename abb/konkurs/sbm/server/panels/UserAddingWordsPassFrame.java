package abb.konkurs.sbm.server.panels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import abb.konkurs.sbm.auth.text.HashAlgorithms;
import abb.konkurs.sbm.server.auth.text.WordsPassManager;

public class UserAddingWordsPassFrame extends JFrame {
	private static final long serialVersionUID = 5586512542984714489L;
	private static final String TITLE = "Change Password";
	private static final String CMD_CHANGE = "SAVE";
	private static final String CMD_EXIT = "EXIT";
	private static final int PassFieldLength = 20;
	private final JLabel passLabel = new JLabel("New password :");
	private final JLabel exampleLabel = new JLabel(
			"Write password words with spaces, they must be "
					+ "real words. EG. \"my mom is amazing\"");
	private JPanel panel;
	private JPanel panel2;
	private JPasswordField passField;
	private JFrame IFrame;
	private String userName;

	public UserAddingWordsPassFrame(String userName) {
		super();
		this.userName = userName;
		setTitle(TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		initElements();
		IFrame = this;
	}

	private void initElements() {
		panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		passField = new JPasswordField(PassFieldLength);
		passLabel.setLabelFor(passField);
		panel.add(passLabel);
		panel.add(passField);
		panel.add(createButtonPanel());
		panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panel2.add(exampleLabel);
		panel.add(panel2);
		this.add(panel);
		this.pack();
		this.setVisible(true);
	}

	protected JComponent createButtonPanel() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JButton okButton = new JButton(CMD_CHANGE);
		JButton exitButton = new JButton(CMD_EXIT);

		okButton.setActionCommand(CMD_CHANGE);
		exitButton.setActionCommand(CMD_EXIT);

		okButton.addActionListener(new MyListener());
		exitButton.addActionListener(new MyListener());

		p.add(okButton);
		p.add(exitButton);
		return p;
	}

	class MyListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals(CMD_CHANGE)) {
				char[] pass;
				pass = passField.getPassword();
				if (pass.length < 6) {
					JOptionPane.showMessageDialog(IFrame,
							"Words password musts be longer than 5 characters");
					return;
				}
				String salt = HashAlgorithms
						.generateSalt(HashAlgorithms.DEFAULT_SALT_LENGTH);
				WordsPassManager.getManager().deleteUser(userName);
				WordsPassManager.getManager().addUser(userName, salt,
						HashAlgorithms.getHashSHA256(new String(pass).toLowerCase() + salt));
				JOptionPane.showMessageDialog(IFrame, "User words password added/changed");
				IFrame.setVisible(false);
				System.out.println("dodalem haslo \""+ new String(pass).toLowerCase()+"\"");
				IFrame.dispose();
			} else if (e.getActionCommand().equals(CMD_EXIT)) {
				IFrame.setVisible(false);
				IFrame.dispose();
			}
		}

	}
}
