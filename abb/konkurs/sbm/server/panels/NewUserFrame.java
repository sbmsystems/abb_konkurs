package abb.konkurs.sbm.server.panels;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import abb.konkurs.sbm.auth.text.HashAlgorithms;
import abb.konkurs.sbm.server.auth.text.PassManager;


/**
 * 
 * @author Alex
 * @version 1.0
 * <p>This class is invoked when user want to make new user</p>
 * 
 */
public class NewUserFrame extends JFrame {
    private static final long serialVersionUID = 5586512542984714489L;
    private static final String TITLE = "Add User";
    private static final String CMD_ADD = "ADD";
    private static final String CMD_EXIT = "EXIT";
    private static final int PassFieldLength = 12;
    private final JLabel passLabel = new JLabel("Password :");
    private final JLabel userLabel = new JLabel("Username :");
    private JPanel panel;
    private JPasswordField passField;
    private JTextField userName;
    private JFrame IFrame;
    private UserManagementPanel parent;
    public NewUserFrame(UserManagementPanel parent) {
        super();
        setTitle(TITLE);
        this.parent = parent;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        initElements();
        IFrame = this;
    }

    private void initElements() {
        panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        passField = new JPasswordField(PassFieldLength);
        userName = new JTextField(PassFieldLength);
        passLabel.setLabelFor(passField);
        userLabel.setLabelFor(userName);

        panel.add(userLabel);
        panel.add(userName);
        panel.add(passLabel);
        panel.add(passField);
        panel.add(createButtonPanel());
        this.add(panel);
        this.pack();
        this.setVisible(true);
    }

    protected JComponent createButtonPanel() {
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEADING));
        JButton okButton = new JButton(CMD_ADD);
        JButton exitButton = new JButton(CMD_EXIT);

        okButton.setActionCommand(CMD_ADD);
        exitButton.setActionCommand(CMD_EXIT);

        okButton.addActionListener(new MyListener());
        exitButton.addActionListener(new MyListener());

        p.add(okButton);
        p.add(exitButton);
        return p;
    }

    class MyListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals(CMD_ADD)) {
                String userN = userName.getText();
                if (!PassManager.getManager().validUserName(userN)) {
                    JOptionPane.showMessageDialog(IFrame, "Wrong username");
                    return;
                }
                if (PassManager.getManager().userAlreadyInDB(userN)) {
                    JOptionPane.showMessageDialog(IFrame, "User already in DB");
                    return;
                }
                char[] pass;
                pass = passField.getPassword();
                if (pass.length < 6) {
                    JOptionPane.showMessageDialog(IFrame,
                            "Password musts be longer than 5 characters");
                    return;
                }
                String salt = HashAlgorithms.generateSalt(HashAlgorithms.DEFAULT_SALT_LENGTH);
                PassManager.getManager().addUser(
                        userN,
                        salt,
                        HashAlgorithms.generateHash(HashAlgorithms.DEFAULT_HASHING_METHOD,
                                new String((pass)) + salt));
                JOptionPane.showMessageDialog(IFrame, "User added");
            	parent.refreshGUI();
                IFrame.setVisible(false);
                IFrame.dispose();
            } else if (e.getActionCommand().equals(CMD_EXIT)) {
            	parent.refreshGUI();
                IFrame.setVisible(false);
                IFrame.dispose();
            }
        }

    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new NewUserFrame(null);
                frame.setTitle("Ramka Testowa");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });

    }

}
