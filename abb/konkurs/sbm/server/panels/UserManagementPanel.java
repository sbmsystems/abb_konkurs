package abb.konkurs.sbm.server.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import abb.konkurs.sbm.server.auth.text.PassManager;

/**
 * 
 * @author Alex
 * @version 1.0
 * <p>Another panel to card layout. This panel is responsible for user management</p>
 */

/*
 * 
 * TODO zrobic kolory guzikow
 */
 
public class UserManagementPanel extends JPanel {
    public static final String TITLE = "Add User";
    private static String commandAddUser = "ADD USER";
    private static String commandDeleteUser = "DELETE USER";
    private static String commandChangePass = "CHANGE PASSWORD";
    private static String commandChangeWordsPass = "CHANGE WORDS PASS";
    private JButton addUser;
    private JButton deleteUser;
    private JButton changePassword;
    private JButton changeWords;
    private JList<String> userList;
    private UserManagementPanel thisPanel;
    /**
     * 
     */
    private static final long serialVersionUID = -5580428690694276088L;

    public UserManagementPanel() {
        super();
        this.thisPanel = this;
        addUser = new JButton(commandAddUser);
        deleteUser = new JButton(commandDeleteUser);
        changePassword = new JButton(commandChangePass);
        changeWords = new JButton(commandChangeWordsPass);
        userList = new JList<>();
        initElements();
    }
    public void refreshGUI(){
    	this.fillUserList();
    	this.repaint();
    }
    private void initElements() {

        /* Przypisanie komend */
        addUser.setActionCommand(commandAddUser);
        deleteUser.setActionCommand(commandDeleteUser);
        changePassword.setActionCommand(commandChangePass);
        changeWords.setActionCommand(commandChangeWordsPass);
        /* Ustawienia rozmiarow i dodanie do panelu */
        /*
        int width = ServerMainFrame.getSTATICPreferedSize().height;
        int height = ServerMainFrame.getSTATICPreferedSize().width;
        addUser.setPreferredSize(new Dimension(height/4,width/3));
        deleteUser.setPreferredSize(new Dimension(height/4,width/3));
        changePassword.setPreferredSize(new Dimension(height/4,width/3));
        changeWords.setPreferredSize(new Dimension(height/4,width/3));
        userList.setPreferredSize(new Dimension(height/4,2*width/3));
        */
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addUser);
        buttonPanel.add(deleteUser);
        buttonPanel.add(changePassword);
        buttonPanel.add(changeWords);
        /* Dodanie actionlistnerow */
        addUser.addActionListener(new MyListener());
        deleteUser.addActionListener(new MyListener());
        changePassword.addActionListener(new MyListener());
        changeWords.addActionListener(new MyListener());
        /* Ustawienie Layoutu */
        this.setLayout(new BorderLayout());
        this.add(buttonPanel, BorderLayout.PAGE_START);
        fillUserList();
        this.add(userList, BorderLayout.PAGE_END);

    }

    /* Cold hard bitch just a kiss on the lips */

    private void fillUserList() {
        String[] arr = PassManager.getManager().getAllUsers();
        if (arr == null){
        	return;
        }
        userList.setListData(arr);
        userList.setSelectedIndex(0);
        userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    private int getIndex() {
        if (userList.isSelectionEmpty())
            return -1;
        return userList.getSelectedIndex();
    }

    class MyListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            // TODO Auto-generated method stub
            if (arg0.getActionCommand().equals(commandAddUser)) {
                JFrame frame = new NewUserFrame(thisPanel);
                frame.setAlwaysOnTop(true);
                frame.setResizable(false);
                fillUserList();
            } else if (arg0.getActionCommand().equals(commandDeleteUser)) {
                if (getIndex() < 0) {
                    return;
                }
                String u = userList.getModel().getElementAt(getIndex());
                PassManager.getManager().deleteUser(u);
                fillUserList();
            } else if (arg0.getActionCommand().equals(commandChangePass)) {
                if (getIndex() < 0) {
                    return;
                }
                String u = userList.getModel().getElementAt(getIndex());
                JFrame frame = new UserChangePasswordFrame(u);
                frame.setAlwaysOnTop(true);
                frame.setResizable(false);
                fillUserList();
            }if (arg0.getActionCommand().equals(commandChangeWordsPass)) {
                if (getIndex() < 0) {
                    return;
                }
                String u = userList.getModel().getElementAt(getIndex());
                JFrame frame = new UserAddingWordsPassFrame(u);
                frame.setAlwaysOnTop(true);
                frame.setResizable(false);
            }

            refreshGUI();
        }

    }

}
