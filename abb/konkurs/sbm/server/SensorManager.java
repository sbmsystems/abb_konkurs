package abb.konkurs.sbm.server;

import java.util.ArrayList;

import abb.konkurs.sbm.sensors.Sensor;

public class SensorManager {
	private ArrayList<Sensor> sensors;
	private static SensorManager instance;
	
	protected SensorManager(){
		sensors = new ArrayList<>();
	}
	public SensorManager getInstance(){
		if (instance == null)
			instance = new SensorManager();
		return instance;
	}
	public void addSensor(Sensor s) {
		synchronized (sensors) {
			boolean flag = false;
			for (int i = 0; i < sensors.size(); i++) {
				if (s.getID() == sensors.get(i).getID()) {
					Sensor tmp = sensors.get(i);
					tmp.setData(s.getData());
					return;
				}
			}
			if (flag == false) {
				sensors.add(s);
			}

		}
	}

	public Sensor[] getAllSensors() {
		synchronized (sensors) {
			if (sensors.size() == 0)
				return null;
			Sensor[] tmpsensors = new Sensor[sensors.size()];
			sensors.toArray(tmpsensors);
			return tmpsensors;
		}
	}


}
