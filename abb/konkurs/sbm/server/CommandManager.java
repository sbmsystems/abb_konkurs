package abb.konkurs.sbm.server;

import java.util.ArrayList;

import abb.konkurs.sbm.server.commands.Command;

/**
 * @author Alex
 * <p>This class implements singleton</p>
 */
public class CommandManager {
	private static CommandManager instance;
	ArrayList <Command> commandList;
	
	protected CommandManager(){
		commandList = new ArrayList<>();
	}
	public CommandManager getInstance(){
		if (instance == null){
			instance = new CommandManager();
		}
		return instance;
	}

	public void removeCommand(int index) {
		synchronized (commandList) {
			commandList.remove(index);
		}
	}

	public void addCommand(Command s) {
		synchronized (commandList) {
			commandList.add(s);
		}
	}

	public Command[] getAllCommands() {
		synchronized (commandList) {
			if (commandList.size() == 0)
				return null;
			Command[] tmpcommands = new Command[commandList.size()];
			commandList.toArray(tmpcommands);
			return tmpcommands;
		}
	}

	
}
