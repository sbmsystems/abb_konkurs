package abb.konkurs.sbm.server;

import java.awt.EventQueue;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import abb.konkurs.sbm.sensors.Sensor;
import abb.konkurs.sbm.server.commands.Command;
import abb.konkurs.sbm.server.net.ServerConnection;

/**
 * 
 * @author Alex
 * @version 1.0
 *          <p>
 *          <b>This class is one of most important class in whole project.</b>
 *          It's first class which is started. Most important const variable
 *          should go there.
 *          </p>
 */

public class ServerMain {
	public static final String VOICE_PASS_FILE = "shadow2";
	public static final String PASS_FILE = "shadow";
	public static final String TWO_INSTANCES_ERROR = "MULTIPLE INSTANCES OF APPLICATION DETECED!!!";
	public static final int SERVER_PORT = 1994;
	public static final ArrayList<UserAuthenticationToken> usersTokens = new ArrayList<>();
	public static ServerMainFrame MAIN_FRAME;
	private static CommandManager cm = new CommandManager();
	private static SensorManager sm = new SensorManager();

	public static void removeCommand(int index) {
		cm.removeCommand(index);
	}

	public static void addCommand(Command s) {
		cm.addCommand(s);
	}

	public static Command[] getAllCommands() {
		return cm.getAllCommands();
	}

	public static void refreshSensors() {
		if (MAIN_FRAME != null) {
			MAIN_FRAME.sensorRefresh();
		}
	}

	public static void addSensor(Sensor s) {
		sm.addSensor(s);
	}

	public static Sensor[] getAllSensors() {
		return sm.getAllSensors();
	}

	public static boolean userTokenIsValid(String username, long id) {
		synchronized (usersTokens) {
			for (UserAuthenticationToken t : usersTokens) {
				if (t.getUsername().equals(username) && t.getToken() == id) {
					t.resetTime();
					return true;
				}
			}
			return false;
		}
	}

	public static synchronized UserAuthenticationToken makeUserAuthenticationToken(
			String username) {
		Random gen = new Random();
		int random = gen.nextInt(100000000);
		UserAuthenticationToken t = new UserAuthenticationToken(username,
				random);
		addUserToken(t);
		return t;
	}

	public static void cleanUserTokens() {
		synchronized (usersTokens) {
			int j = 0;
			for (int i = 0; i < usersTokens.size(); i++, j++) {
				if (usersTokens.get(j).tokenStillValid()) {
					usersTokens.remove(j);
					j--;
				}
			}
		}
	}

	private static synchronized void addUserToken(UserAuthenticationToken t) {
		synchronized (usersTokens) {
			usersTokens.add(t);
		}
	}

	/**
	 * <p>
	 * This method check if PORT is avaliable
	 * </p>
	 * <p>
	 * False result means that 1.There is another instance of application
	 * running on that port or another app is occupying port, finally there
	 * might be also another error
	 * </p>
	 * 
	 * @param port
	 * @return
	 */
	public static boolean portIsAvaliable(int port) {
		DatagramSocket ds = null;
		ServerSocket ss = null;
		try {
			ss = new ServerSocket(port);
			ss.setReuseAddress(true);
			ds = new DatagramSocket(port);
			ds.setReuseAddress(true);
			return true;
		} catch (Exception e) {
		} finally {
			if (ds != null) {
				ds.close();
			}

			if (ss != null) {
				try {
					ss.close();
				} catch (Exception e) {
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		if (!portIsAvaliable(SERVER_PORT)) {
			JFrame frame = new JFrame("Error");
			JOptionPane.showMessageDialog(frame, TWO_INSTANCES_ERROR, "ERROR",
					JOptionPane.ERROR_MESSAGE);
			frame.dispose();
			System.exit(-1);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MAIN_FRAME = new ServerMainFrame();
			}
		});
		ExecutorService executor = Executors.newCachedThreadPool();
		Runnable serverconnectoin = new ServerConnection();
		executor.execute(serverconnectoin);
		Runnable refreshingSensorGui = new RefreshingThread();
		executor.execute(refreshingSensorGui);

	}

}