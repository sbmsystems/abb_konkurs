package abb.konkurs.sbm.server;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import abb.konkurs.sbm.server.commands.Command;

public class CommandManagementFrame extends JFrame {
	private static final long serialVersionUID = -1427039486670853257L;
	private JList<String> avaliableCommands = new JList<String>();
	private JList<String> commandsIncluded = new JList<String>();
	private JButton addCommandButton;
	private JButton removeCommandButton;
	private JButton saveCommandButton;
	private JButton abortButton;
	private static String addCommand = "Add command";
	private static String saveCommand = "Save command";
	private static String abort = "Abort";
	private static String removeCommand = "Remove command";
	private JTextField nameField;
	private Command[] commandsLocalCopy = null;
	private JPanel buttonPanel1;
	private JPanel buttonPanel2;
	private JPanel jListPanel;
	private JPanel namePanel;

	public CommandManagementFrame() {
		super();
		setTitle("New command");
		setPreferredSize(new Dimension(550, 450));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initButtons();
		initJList();
		initText();
		initGui();
	}

	private void initButtons() {
		/* Init buttons with images */
		addCommandButton = new JButton(addCommand, new ImageIcon(
				"Ikony/Alarm-Plus-icon.png"));
		removeCommandButton = new JButton(removeCommand, new ImageIcon(
				"Ikony/Alarm-Minus-icon.png"));
		saveCommandButton = new JButton(saveCommand, new ImageIcon(
				"Ikony/Alarm-Tick-icon.png"));
		abortButton = new JButton(abort, new ImageIcon(
				"Ikony/Alarm-Error-icon.png"));
		/*setting size*/
		addCommandButton.setPreferredSize(new Dimension(260, 100));
		removeCommandButton.setPreferredSize(new Dimension(260, 100));
		saveCommandButton.setPreferredSize(new Dimension(240, 100));
		abortButton.setPreferredSize(new Dimension(240, 100));
		/* Adding listener */
		// TODO
		/* Adding to panels */
		buttonPanel1 = new JPanel(new FlowLayout());
		buttonPanel2 = new JPanel(new FlowLayout());
		buttonPanel1.add(addCommandButton);
		buttonPanel1.add(removeCommandButton);
		buttonPanel2.add(saveCommandButton);
		buttonPanel2.add(abortButton);
	}

	private void initJList() {
		avaliableCommands = new JList<String>();
		commandsIncluded = new JList<String>();
		initAvaliableList();

		avaliableCommands.setSelectedIndex(0);
		avaliableCommands.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		commandsIncluded.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		avaliableCommands.setSize(240, 200);
		commandsIncluded.setSize(240, 200);
		
		commandsIncluded.setVisibleRowCount(10);
		avaliableCommands.setVisibleRowCount(10);
		
		jListPanel = new JPanel(new FlowLayout());
		
		jListPanel.add(avaliableCommands);
		jListPanel.add(commandsIncluded);
	}

	private void initText() {
		nameField = new JTextField("Name:", 30);
		namePanel = new JPanel(new FlowLayout());
		namePanel.add(nameField);
	}

	private void initAvaliableList() {
		if (commandsLocalCopy == null) {
			commandsLocalCopy = ServerMain.getAllCommands();
		}
		if (commandsLocalCopy == null)
			return;
		String[] arr = new String[commandsLocalCopy.length];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = commandsLocalCopy[i].getDesc();
		}
		avaliableCommands.setListData(arr);
	}

	private void initGui() {
		JPanel finalPanel = new JPanel();
		finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.PAGE_AXIS));
		
		finalPanel.add(buttonPanel2);
		finalPanel.add(namePanel);
		finalPanel.add(jListPanel);
		finalPanel.add(buttonPanel1);
		this.add(finalPanel);
		this.pack();
		this.setVisible(true);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new CommandManagementFrame();
				frame.setTitle("Ramka Testowa");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});

	}

}
