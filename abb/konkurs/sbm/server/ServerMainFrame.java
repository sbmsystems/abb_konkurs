package abb.konkurs.sbm.server;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import abb.konkurs.sbm.server.cards.CardLayoutCard;
import abb.konkurs.sbm.server.cards.CommandCard;
import abb.konkurs.sbm.server.cards.SensorCard;
import abb.konkurs.sbm.server.cards.UserCard;

/** * 
 * @author Alex
 * @version 1.0
 * <p>This is main frame of whole application. Layout is based on card layout</p> 
 */
public class ServerMainFrame extends JFrame{
    private static final long serialVersionUID = 4505138831357006665L;
    private static String CARD_KINECT = "Kinect";
    private static String CARD_GESTURE = "Gesture";
    private static String Title = "Little Home APP by SBM Systems";
    private final static int DEFAULT_WIDTH = 600;
    private final static int DEFAULT_HEIGHT = 350;
    private final static int extraWindowWidth = 100;
    private JPanel kinectCard;
    private UserCard userCard;
    private JPanel gestureCard;
    private SensorCard sensorCard;
    private CardLayoutCard commandsCard;
    public ServerMainFrame(){
        super();
        setTitle(Title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addComponentToPane(this.getContentPane());
        pack();
        setVisible(true);
    }
    /**
     * <p>This method will print the simplest dialog window</p>
     * @param message
     */
    public void printDialog(String message){
       JOptionPane.showMessageDialog(this, message); 
    }
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(DEFAULT_WIDTH,DEFAULT_HEIGHT);
    }
    /**
     * @author SUPER_HACKER
     * @return dimension
     */
    public static Dimension getSTATICPreferedSize(){
        return new Dimension(DEFAULT_WIDTH,DEFAULT_HEIGHT);
    }
    @SuppressWarnings("serial")
    public void addComponentToPane(Container pane) {
        JTabbedPane tabbedPane = new JTabbedPane();

        kinectCard = new JPanel() {
            public Dimension getPreferredSize() {
                Dimension size = getPreferredSize();
                size.width += extraWindowWidth;
                return size;
            }
        };

        userCard = new UserCard();
        sensorCard = new SensorCard();
        gestureCard = new JPanel();
        
        commandsCard = new CommandCard();

        tabbedPane.addTab(userCard.getTitle(), userCard);
        tabbedPane.addTab(CARD_KINECT, kinectCard);
        tabbedPane.addTab(CARD_GESTURE, gestureCard);
        tabbedPane.addTab(sensorCard.getTitle(), sensorCard);
        tabbedPane.addTab(commandsCard.getTitle(), commandsCard);
        pane.add(tabbedPane, BorderLayout.CENTER);
    }
    public void userRefresh(){
    	if (userCard != null )
    	userCard.refreshUsers();
    }
    public void sensorRefresh(){
    	if (sensorCard != null){
    		sensorCard.refreshSensor();
    	}
    }
 
    
    
}
