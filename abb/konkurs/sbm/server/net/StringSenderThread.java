package abb.konkurs.sbm.server.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class StringSenderThread extends Thread {
	Socket mySocket;
	PrintWriter in = null;
	BufferedReader out = null;
	private String toSend;
	public StringSenderThread(Socket socket, String toSend) {
		super();
		this.mySocket = socket;
		this.toSend = toSend;
	}
	public void run() {
		System.out.println("## Processing data packagage");
		if (mySocket.isClosed()) {
			System.err.println("## SOCKET IS CLOSED ....");
			return;
		}
		try {
			in = new PrintWriter(mySocket.getOutputStream(), true);
			out = new BufferedReader(new InputStreamReader(
					mySocket.getInputStream()));

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (in == null || out == null)
			return;
		try {
			in.println(toSend);
		} finally {
			try {
				mySocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
