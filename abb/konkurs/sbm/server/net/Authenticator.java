package abb.konkurs.sbm.server.net;
/**
 * 
 * 
 * @author Alex
 *
 */
public interface Authenticator {
    /**
     * 
     * @return -1 if user name is not valid, 0 if wrong pass/image itp, 1 if logged ok
     */
    public int tryAuthenticate();
}
