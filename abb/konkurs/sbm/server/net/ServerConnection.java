package abb.konkurs.sbm.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import abb.konkurs.sbm.server.ServerMain;
/**
 * 
 * @author Alex
 * <p>
 */
public class ServerConnection implements Runnable {
    private ExecutorService threads;
    private ServerSocket socket;
    private boolean serverIsAlive;
    {
        serverIsAlive = true;
        threads = Executors.newFixedThreadPool(20);
    }

    public ServerConnection() {
        try {
            socket = new ServerSocket(ServerMain.SERVER_PORT);
        } catch (IOException e) {
            e.printStackTrace();
            serverIsAlive = false;
        }
    }

    public void run() {
        while (serverIsAlive) {
            Socket clientSocket = null;
            try {
                clientSocket = socket.accept();
                System.out.println("Dostalem info");
            } catch (IOException e) {
                System.err.println("Exception has risen when clientSocket connect");
                e.printStackTrace();
            } finally {
                if (clientSocket != null)
                    threads.execute(new ClientThread(clientSocket));
            }
        }
    }
    
    public synchronized void stopServer() {
        serverIsAlive = false;
    }

    public synchronized boolean isAlive() {
        return serverIsAlive;
    }

}
