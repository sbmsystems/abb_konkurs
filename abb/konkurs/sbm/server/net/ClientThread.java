package abb.konkurs.sbm.server.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.net.states.OurNetworkProtocolState;
import abb.konkurs.sbm.server.net.states.RecognitionState;

public class ClientThread extends Thread {
	Socket mySocket;
	PrintWriter in = null;
	BufferedReader out = null;
	OurNetworkProtocolState currentState;

	public ClientThread(Socket socket) {
		super();
		this.mySocket = socket;
		currentState = new RecognitionState();
	}

	public void setState(OurNetworkProtocolState s) {
		currentState = s;
	}

	public InetSocketAddress getInetFromSocket() {
		InetSocketAddress tmp = new InetSocketAddress(
				mySocket.getInetAddress(), ServerMain.SERVER_PORT);
		return tmp;
	}

	public void run() {
		System.out.println("## Processing data packagage");
		if (mySocket.isClosed()) {
			System.err.println("## SOCKET IS CLOSED ....");
			return;
		}
		try {
			in = new PrintWriter(mySocket.getOutputStream(), true);
			out = new BufferedReader(new InputStreamReader(
					mySocket.getInputStream()));

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (in == null || out == null)
			return;
		try {
			while (currentState != null) {
				System.out.println("## Current state is "+currentState.getClass().getName());
				currentState.handle(this, in, out);
			}
		} catch (Exception e) {

		} finally {
			try {
				mySocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
