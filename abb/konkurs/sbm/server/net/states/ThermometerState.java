package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.PrintWriter;

import abb.konkurs.sbm.sensors.ThermometerSensor;
import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.net.ClientThread;

public class ThermometerState implements OurNetworkProtocolState {
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String id;
		String temperature;
		try {
			id = out.readLine();
			temperature = out.readLine();
			int idNumber = new Integer (id);
			
			ServerMain.addSensor(new ThermometerSensor(idNumber,
						temperature, ct.getInetFromSocket()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ct.setState(null);
		}

	}
}
