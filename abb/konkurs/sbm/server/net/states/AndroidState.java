package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;

/**
 * @author Kamil
 * <p>This is first android state.</p>
 */

public class AndroidState implements OurNetworkProtocolState {

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String subtype = null;
		try {
			subtype = out.readLine();
			if (subtype.equalsIgnoreCase("Authenticator")) {
				ct.setState(new AndroidAuthenticatorState());
			} else if (subtype.equalsIgnoreCase("Command")) {
				ct.setState(new AndroidCommandState());
			} else if (subtype.equalsIgnoreCase("Request")) {
				ct.setState(new AndroidRequestState());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (subtype == null) {
				ct.setState(null);
			}
		}
	}

}
