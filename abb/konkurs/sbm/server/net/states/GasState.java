package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.PrintWriter;

import abb.konkurs.sbm.sensors.GasSensor;
import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.net.ClientThread;

public class GasState implements OurNetworkProtocolState {
	public void handle(ClientThread ct, PrintWriter in ,BufferedReader out ){
		String id;
		String gasDetected;
		try {
			id = out.readLine();
			gasDetected = out.readLine();
			if (gasDetected == "1") {
				ServerMain.addSensor(new GasSensor(new Integer(id),
						"Gas detected", ct.getInetFromSocket()));
			} else {
				ServerMain.addSensor(new GasSensor(new Integer(id),
						"Gas not detected", ct.getInetFromSocket()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ct.setState(null);
		}

	}

}
