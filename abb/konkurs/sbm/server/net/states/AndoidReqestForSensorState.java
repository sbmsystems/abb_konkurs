package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.PrintWriter;

import abb.konkurs.sbm.sensors.Sensor;
import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.net.ClientThread;

public class AndoidReqestForSensorState implements OurNetworkProtocolState {
	public static String BEGIN = "BEGIN";
	public static String END = "END";

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String subtype = null;
		try {
			in.println(BEGIN);
			Sensor[] sen = ServerMain.getAllSensors();
			for (int i = 0 ; i < sen.length;i ++){
				in.println(sen[i].getID());
				in.println(sen[i].getDesc());
				in.println(sen[i].getData());
			}
			in.println(END);
		} finally {
			if (subtype == null) {
				ct.setState(null);
			}
		}
	}

}
