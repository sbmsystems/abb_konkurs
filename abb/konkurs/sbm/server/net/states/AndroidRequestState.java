package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;

public class AndroidRequestState implements OurNetworkProtocolState {

	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String subtype = null;
		try {
			subtype = out.readLine();
			if (subtype.equalsIgnoreCase("Sensors")) {
				ct.setState (new AndoidReqestForSensorState());
			} else if (subtype.equalsIgnoreCase("Alive")) {
				ct.setState(null);
			} else if (subtype.equalsIgnoreCase("GiveToken")) {
				ct.setState(null);
			} else
				subtype = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (subtype == null) {
				ct.setState(null);
			}

		}
	}

}
