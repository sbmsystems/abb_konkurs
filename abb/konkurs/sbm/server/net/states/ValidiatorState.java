package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.net.ClientThread;

public class ValidiatorState implements OurNetworkProtocolState {

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String userName = null;
		String userToken = null;
		long token;
		try {
			userName = out.readLine();
			userToken = out.readLine();
			token = new Integer(userToken);
			if( ServerMain.userTokenIsValid(userName, token)){
				in.println("SUCCESS");
				ct.setState(null);
			}
			else{
				in.println("FAIL");
				ct.setState(null);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println(userName +"\n"+ userToken);
			if (userName == null || userToken == null) {
				ct.setState(null);
			}
			
		}
	}

}
