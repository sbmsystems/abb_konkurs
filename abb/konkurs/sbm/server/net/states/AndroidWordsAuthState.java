package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.auth.text.HashAlgorithms;
import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.UserAuthenticationToken;
import abb.konkurs.sbm.server.auth.text.PassManager;
import abb.konkurs.sbm.server.auth.text.WordsPassManager;
import abb.konkurs.sbm.server.net.ClientThread;

public class AndroidWordsAuthState implements OurNetworkProtocolState {

public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String username = null;
		String password = null;
		try {
			username = out.readLine();
			password = out.readLine();
			password = password.toLowerCase();
			System.out.println("## pass \"" + password + "\" username + "+ username);
			System.out.println("## USER: " +PassManager.getManager().userHash(username));
			System.out.println("## USER: " +PassManager.getManager().userHash(username));
			System.out.println("## HASH: " + HashAlgorithms.getHashSHA256(password + PassManager.getManager().userSalt(username)));
			if (!WordsPassManager.getManager().userAlreadyInDB(username)) {
				in.println("FAIL");
				in.println("Wrong username, userNames are case SeNsItIvE");
				System.out.println("## Wrong username, userNames are case SeNsItIvE");
			} /*else if (ServerMain.userAlreadyHasActiveToken(username)) {
				in.println("FAIL");
				in.println("Username user already logged in");
				System.out.println("## Username user already logged in");
			}*/ else if (WordsPassManager
					.getManager()
					.userHash(username)
					.equals(HashAlgorithms.getHashSHA256(password
							+ WordsPassManager.getManager().userSalt(username)))) {
				UserAuthenticationToken t = ServerMain.makeUserAuthenticationToken(username);
				in.println("SUCCESS");
				in.println(t.getToken());
				System.out.println("##Everything is OK");
			}
			else{
				in.println("FAIL");
				in.println("Wrong password");
				System.out.println("## Wrong password");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
				ct.setState(null);
		}
	}
}
