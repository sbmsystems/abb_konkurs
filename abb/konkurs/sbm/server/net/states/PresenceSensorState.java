package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.PrintWriter;

import abb.konkurs.sbm.sensors.PresenceSensor;
import abb.konkurs.sbm.server.ServerMain;
import abb.konkurs.sbm.server.net.ClientThread;

public class PresenceSensorState implements OurNetworkProtocolState {
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String id;
		String temp;
		try {
			id = out.readLine();
			temp = out.readLine();
			if (temp == "1") {
				ServerMain.addSensor(new PresenceSensor(new Integer(id),
						"Motion detected", ct.getInetFromSocket()));
			} else {
				ServerMain.addSensor(new PresenceSensor(new Integer(id),
						"Motion not detected", ct.getInetFromSocket()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ct.setState(null);
		}

	}
}
