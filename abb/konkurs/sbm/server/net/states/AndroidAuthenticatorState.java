package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;

public class AndroidAuthenticatorState implements OurNetworkProtocolState {

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String subtype = null;
		try {
			subtype = out.readLine(); if (subtype.equalsIgnoreCase("Text")) {
				ct.setState(new AndroidTextAuthState());
			} else if (subtype.equalsIgnoreCase("Voice")) {
				ct.setState(new AndroidWordsAuthState());
			} else if (subtype.equalsIgnoreCase("Image")) {
				ct.setState(null);
			}
			else if (subtype.equalsIgnoreCase("Validator")) {
				ct.setState(new ValidiatorState());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (subtype == null) {
				ct.setState(null);
			}

		}

	}
}