package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;

public class AndroidCommandState implements OurNetworkProtocolState {

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String subtype = null;
		try {
			subtype = out.readLine();
			if (subtype.equalsIgnoreCase("Text")) {
				//TODO
				ct.setState(null);
			} else if (subtype.equalsIgnoreCase("Words")) {
				// TODO
				ct.setState(null);
			} else if (subtype.equalsIgnoreCase("Image")) {
				// TODO
				ct.setState(null);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (subtype == null) {
				ct.setState(null);
			}

		}
	}

}
