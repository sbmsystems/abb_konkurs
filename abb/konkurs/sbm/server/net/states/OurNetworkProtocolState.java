package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;
/**
 * 
 * @author Alex
 * @version 1.0
 * <p>This interface is state pattern.</p>
 */
public interface OurNetworkProtocolState {
    public void handle(ClientThread ct, PrintWriter in ,BufferedReader out );
}

