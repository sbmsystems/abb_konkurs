package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;

/**
 * 
 * @author Alex
 * @version 1.0
 *          <p>
 *          This state is responsible for reading first line of message, then
 *          setting new state of ClientThread respectively.
 *          </p>
 *          <p>
 *          NOTE: new State will be null if first line of message will be
 *          unrecognized
 *          </p>
 */

public class RecognitionState implements OurNetworkProtocolState {

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String type = null;
		try {
			type = out.readLine();
			if (type.equalsIgnoreCase("Android")) {
				ct.setState(new AndroidState());
			} else if (type.equalsIgnoreCase("Arduino")) {
				// TODO
				ct.setState(new ArduinoState());
			} else {
				type = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (type == null) {
				ct.setState(null);
			}
		}

	}

}
