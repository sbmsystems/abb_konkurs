package abb.konkurs.sbm.server.net.states;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import abb.konkurs.sbm.server.net.ClientThread;


/**
 * @author Kamil
 * <p>This is first arduino state.</p>
 */
public class ArduinoState implements OurNetworkProtocolState {

	@Override
	public void handle(ClientThread ct, PrintWriter in, BufferedReader out) {
		String subtype = null;
		try {
			subtype = out.readLine();
			if (subtype.equalsIgnoreCase("Thermometer")){
				ct.setState(new ThermometerState());
			}else if (subtype.equalsIgnoreCase("Gas")){
				
				ct.setState(new GasState());
			}else if (subtype.equalsIgnoreCase("Presence Sensor")){
				
				ct.setState(new PresenceSensorState());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if (subtype == null){
				ct.setState(null);
			}
		}
	
	}

}
