package abb.konkurs.sbm.server.net;

import abb.konkurs.sbm.auth.text.HashAlgorithms;
import abb.konkurs.sbm.server.auth.text.PassManager;

public class TextAuthenticator implements Authenticator {
    private String pass;
    private String username;

    TextAuthenticator(String username, String pass) {
        this.pass = pass;
        this.username = username;
    }

    @Override
    public int tryAuthenticate() {
        if (!PassManager.getManager().userAlreadyInDB(username)) {
            return -1;
        }
        String hash = PassManager.getManager().userHash(username);
        String salt = PassManager.getManager().userSalt(username);
        int hashingMethod = new Integer(PassManager.getManager().userHashMethod(username));
        if (hash.equals(HashAlgorithms.generateHash(hashingMethod, new String (pass + salt))))
            return 1;
        return 0;
    }

}
