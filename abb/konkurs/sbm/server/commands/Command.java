package abb.konkurs.sbm.server.commands;

public abstract class Command {
	private String desc;
	public void setDescription(String desc){
		this.desc = desc;
	}
	public String getDesc(){
		return desc;
	}
	public Command(String desc) {
		this.desc = desc;
	}
	public abstract void performCommand();
	public abstract boolean isLeaf();
	public void addChild(Command child){
		throw new UnsupportedOperationException("Not implemented!!!");
	}
	public void addChild (int index, Command child){
		throw new UnsupportedOperationException("Not implemented!!!");
	}

	public void removeChild (int index){
		throw new UnsupportedOperationException("Not implemented!!!");
	}

}
