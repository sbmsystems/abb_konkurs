package abb.konkurs.sbm.server.commands;

import java.util.ArrayList;
/**
 * 
 * @author Alex
 * @version 1.0
 * <h1>Basic command composite</h1><p>This class implements composite
 * pattern.</p>
 */
public class BasicCommandComposite extends Command {

	ArrayList<Command> commands;

	public BasicCommandComposite(String desc) {
		super(desc);
		commands = new ArrayList<>();
	}


	

	@Override
	public void performCommand() {
		System.out.println("Basic command composite executed");
		for (Command c : commands) {
			c.performCommand();
		}
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

	@Override
	public void addChild(Command child) {
		commands.add(child);
	}

	@Override
	public void addChild(int index, Command child) {
		commands.add(index, child);
	}

	@Override
	public void removeChild(int index) {
		if (index > 0 && index < commands.size())
			commands.remove(index);
	}

}
