package abb.konkurs.sbm.server.commands;

public class TurnOffCommand extends Command {

	public TurnOffCommand(String desc) {
		super(desc);
	}

	@Override
	public void performCommand() {
		System.out.println("Dummy turn off command executed");
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}
