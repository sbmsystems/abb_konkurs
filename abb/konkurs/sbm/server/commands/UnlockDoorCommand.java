package abb.konkurs.sbm.server.commands;

public class UnlockDoorCommand extends Command {

	public UnlockDoorCommand(String desc) {
		super(desc);
	}

	@Override
	public void performCommand() {
		System.out.println("Door Unlocked");
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}
