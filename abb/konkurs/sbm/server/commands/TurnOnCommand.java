package abb.konkurs.sbm.server.commands;

public class TurnOnCommand extends Command {

	public TurnOnCommand(String desc) {
		super(desc);
	}

	@Override
	public void performCommand() {
		System.out.println("Dummy turn on executed");
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}
