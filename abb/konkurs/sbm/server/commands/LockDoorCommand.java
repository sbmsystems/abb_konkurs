package abb.konkurs.sbm.server.commands;

public class LockDoorCommand extends Command {

	public LockDoorCommand(String desc) {
		super(desc);
	}

	@Override
	public void performCommand() {
		System.out.println("Door locked");
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}
