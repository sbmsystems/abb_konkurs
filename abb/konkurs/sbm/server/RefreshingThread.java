package abb.konkurs.sbm.server;

public class RefreshingThread implements Runnable {
	private volatile int counter = 0;
	@Override
	public void run() {
		
		while (true){
			counter ++;
			if (counter > 10){
				ServerMain.refreshSensors();
				counter = 0;
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
