package abb.konkurs.sbm.server.cards;

import abb.konkurs.sbm.server.panels.SensorManagementPanel;

public class SensorCard extends CardLayoutCard {
	private static final long serialVersionUID = 1L;
	private static final SensorManagementPanel m = new SensorManagementPanel();
	private static String TITLE = "Sensors";

	public SensorCard() {
		this.add(m);
		this.setTitle(TITLE);
	}
	public void refreshSensor() {
		m.refreshGUI();
	}
}
