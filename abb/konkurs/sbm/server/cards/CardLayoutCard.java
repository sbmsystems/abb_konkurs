package abb.konkurs.sbm.server.cards;

import java.awt.Dimension;

import javax.swing.JPanel;

import abb.konkurs.sbm.server.ServerMainFrame;

/**
 * 
 * @author Alex
 * @version 1.0
 *          <p>
 *          All cards from cardlayout used in ServerMainFrame extends this class
 *          </p>
 *          <p>
 *          It contains some basic methods, and constants
 *          </p>
 */

public class CardLayoutCard extends JPanel {

	private final static int extraWindowWidth = 10;
	private static final long serialVersionUID = 1L;
	public String title = "NO TITLE";
	public String  getTitle(){
		return title;
	}
	protected void setTitle(String newTitle){
		title = newTitle;
	}
	public Dimension getPreferredSize() {
		Dimension size = ServerMainFrame.getSTATICPreferedSize(); 
		size.width += extraWindowWidth;
		return size;
	}
}
