package abb.konkurs.sbm.server.cards;

import abb.konkurs.sbm.server.panels.UserManagementPanel;

public class UserCard extends CardLayoutCard {
	private static final long serialVersionUID = 1L;
	private static final UserManagementPanel m = new UserManagementPanel();
	private static String TITLE = "Users";

	public UserCard() {
		this.add(m);
		this.setTitle(TITLE);
	}
	public void refreshUsers() {
		m.refreshGUI();
	}
}
