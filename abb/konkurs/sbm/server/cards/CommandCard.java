package abb.konkurs.sbm.server.cards;

import abb.konkurs.sbm.server.panels.CommandManagementPanel;

public class CommandCard extends CardLayoutCard{
	private static final long serialVersionUID = 1L;
	private static final CommandManagementPanel m = new CommandManagementPanel();
	private static String TITLE = "Commands";

	public CommandCard() {
		this.add(m);
		m.setVisible(true);
		this.setTitle(TITLE);
	}
	

}
