package abb.konkurs.sbm.sensors;

import java.net.InetSocketAddress;

/**
 * 
 * @author Alex
 * <p>Basic interface for Sensors</p>
 */
public interface Sensor {
	public InetSocketAddress getISA();
    public int getID();
    public String getDesc();
    public String getData();
    public void setData(String data);
    public void refreshState();
}