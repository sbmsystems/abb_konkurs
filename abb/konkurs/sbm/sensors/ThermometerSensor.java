package abb.konkurs.sbm.sensors;

import java.net.InetSocketAddress;

public class ThermometerSensor implements Sensor {
	private int id;
	private InetSocketAddress myISA;
	private String value;
	private String desc;

	public ThermometerSensor(int id, String value, InetSocketAddress myISA) {
		this.id = id;
		this.value = value;
		this.myISA = myISA;
		this.desc = "Thermometer Sensor";
	}

	public void setData(String data) {
		this.value = data;
	}

	public int getID() {
		return this.id;
	}

	public String getData() {
		return this.value;
	}

	public String getDesc() {
		return this.desc;
	}

	public void refreshState() {

	}

	public InetSocketAddress getISA() {
		return myISA;
	}
}
